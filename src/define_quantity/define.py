# -*- coding: utf-8 -*-

# SPDX-FileCopyrightText: 2024 Kalle Koskinen <kvk@iki.fi>
#
# SPDX-License-Identifier: MIT

from eseries import find_nearest, find_greater_than_or_equal
from pint import UnitRegistry

ureg = UnitRegistry(autoconvert_offset_to_baseunit=True)

preferred_units = [
    ureg.hertz,
    ureg.second,
    ureg.C,
    ureg.volt,
    ureg.ampere,
    ureg.watt,
    ureg.ohm,
    ureg.farad,
    ureg.henry,
    ureg.kelvin/ureg.watt,
]

one_volt = ureg.Quantity('1 V')


def define(description: str,
           quantity: str,
           series: int = None,
           e_series_dir: str = "nearest") -> ureg.Quantity:

    quantity = ureg.Quantity(quantity).to_base_units()

    magnitude = quantity.magnitude
    if series is not None:
        match e_series_dir:
            case "nearest":
                magnitude = find_nearest(series, magnitude)
            case "ge":
                magnitude = find_greater_than_or_equal(series, magnitude)

    # Pint to_preferred() is slow as molasses
    unit = quantity.units
    for preferred_unit in preferred_units:
        if quantity.dimensionality == preferred_unit.dimensionality:
            unit = preferred_unit
            break

    quantity = ureg.Quantity(magnitude, unit)

    match quantity.units:
        case ureg.kelvin:
            print(f'{description} {quantity.to("degC"):.2f~#P}')
        case _:
            print(f'{description} {quantity:.2f~#P}')

    return (quantity)


# Test code
if __name__ == '__main__':
    V_i = define('Input voltage          ', '3.2V')
    I_o = define('Output current         ', '1 uA')
    L   = define('Main switching inductor', '1 mH')
    T   = define('Ambient temperature    ', '30 degC')
    T_c = define('Cold temperature       ', '10 degK')
