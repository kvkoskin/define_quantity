# -*- coding: utf-8 -*-

# SPDX-FileCopyrightText: 2024 Kalle Koskinen <kvk@iki.fi>
#
# SPDX-License-Identifier: MIT

from .define import define, one_volt
