# Define Quantity

Python library for defining quantity variables with simple annotation.
Goal is to help make self documenting scripts
for calculations with physical quantities.
